/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.modelo;

/**
 *
 * @author Fabricio
 */
public class Datos {

    private String nombre;
    private String apodo;
 
    private int oro;
    private int vidas;

    public static final String[] ETIQUETA_ESTADISTICA = {"Nombre", "Apodo", "Oro", "Vidas"};

    public Datos(String nombre, String apodo, int oro, int vidas) {

        this.nombre = nombre;
        this.apodo = apodo;
        this.oro = oro;
        this.vidas = vidas;

    }

    public Datos() {
    }

    public String setDatosEstadistica(int indice) {
        switch (indice) {
            case 0:
                return this.getNombre();
            case 1:
                return this.getApodo();
            case 2:
                return Integer.toString(this.getOro());
            case 3:
                return Integer.toString(this.getVidas());
        }
        return null;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }

    public int getOro() {
        return oro;
    }

    public void setOro(int oro) {
        this.oro = oro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.modelo;

import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.ImageIcon;

/**
 *
 * @author Fabricio
 */
public class Objeto extends Elemento {

    private int direccion;
 

    public Objeto(ImageIcon img, int x, int y, int direccion) {
        super(img, x, y);
        this.direccion = direccion;
     
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

    public void dibujar(Graphics g) {
        getImagen().paintIcon(null, g, getX(), getY());

    }

 
    public Rectangle getBounds() {
        return new Rectangle(getX(), getY(), getImagen().getIconWidth(), getImagen().getIconWidth());
    }

    public void mover() {
        switch (direccion) {
            case 0:
                if (getY() < 800) {
                    setY(getY() + 50);
                } else {
                    setY(0);
                }
                break;
            case 1:
                if (getY() < 800) {
                    setY(getY() + 1);
                } else {
                    setY(0);
                }
            case 2:
                if (getY() < 800) {
                    setY(getY() + 1);
                } else {
                    setY(0);
                }
            case 3:
                if (getY() < 850) {
                    setY(getY() + 12);
                } else {
                    setY(0);
                }
                break;
            case 4:
                if (getY() < 850) {
                    setY(getY() + 11);
                } else {
                    setY(0);
                }
            case 5:
                if (getY() < 850) {
                    setY(getY() + 10);
                } else {
                    setY(0);
                }

        }

    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/File.java to edit this template
 */
package ucr.ac.cr.modelo;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Fabricio
 */
public class RegistroDatosXML {

    private Element raiz;
    private Document documento;
    private ArrayList<Datos> listaDato;
    private File archivo;

    public RegistroDatosXML() {
        this.archivo = new File("datos.xml");
        if (archivo.exists()) {
            leerXML();
        } else {
            this.raiz = new Element("datos");
            this.documento = new Document(raiz);
        }
    }

    public void guardarXML() {
        try {
            XMLOutputter xmlOut = new XMLOutputter();
            Charset cs = Charset.forName("UTF-8");
            xmlOut.output(documento, new PrintWriter(archivo));
        } catch (IOException ex) {
            System.out.println("Error al guardar el archivo");
        }

    }

    public String agregar(Datos datos) {
        Datos dato = (Datos) datos;
        Element eDatos = new Element("datos");
        Attribute aNombre = new Attribute("nombre", datos.getNombre());

        Element eApodo = new Element("apodo");
        eApodo.addContent(datos.getApodo());

        Element eOro = new Element("oro");
        eOro.addContent(Integer.toString(datos.getOro()));

        Element eVida = new Element("vida");
        eVida.addContent(Integer.toString(datos.getVidas()));

        eDatos.setAttribute(aNombre);
        eDatos.addContent(eApodo);
        eDatos.addContent(eOro);
        eDatos.addContent(eVida);

        raiz.addContent(eDatos);
        this.guardarXML();
        return "Dato agregado con exito";

    }

    public void leerXML() {

        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            saxBuilder.setIgnoringElementContentWhitespace(true);
            this.documento = saxBuilder.build(archivo);
            this.raiz = this.documento.getRootElement();
            this.listaDato = new ArrayList<>();

            List<Element> eListaDatos = this.raiz.getChildren("datos");
            for (Element eDatos : eListaDatos) {
                Datos datos = new Datos();
                datos.setNombre(eDatos.getAttributeValue("nombre"));
                datos.setApodo(eDatos.getChildText("apodo"));
                datos.setOro(Integer.parseInt(eDatos.getChildText("oro")));
                datos.setVidas(Integer.parseInt(eDatos.getChildText("vida")));

                this.listaDato.add(datos);
            }

        } catch (JDOMException ex) {
            Logger.getLogger(RegistroDatosXML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RegistroDatosXML.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
 

    public String[][] getDatosTabla() {
        leerXML();
        String[][] matrizTabla = new String[this.listaDato.size()][Datos.ETIQUETA_ESTADISTICA.length];
        for (int f = 0; f < this.listaDato.size(); f++) {
            for (int c = 0; c < matrizTabla[0].length; c++) {
                matrizTabla[f][c] = this.listaDato.get(f).setDatosEstadistica(c);
            }
        }
        return matrizTabla;
    }

    public Datos buscar(String nombre) {
        List<Element> eListaDatos = this.raiz.getChildren();
        for (Element eDatos : eListaDatos) {
            if (eDatos.getAttributeValue("nombre").equals(nombre)) {
                return new Datos(eDatos.getAttributeValue("nombre"), eDatos.getChildText("apodo"), Integer.parseInt(eDatos.getChildText("oro")), Integer.parseInt(eDatos.getChildText("vida")));
            }
        }
        return null;
    }
}

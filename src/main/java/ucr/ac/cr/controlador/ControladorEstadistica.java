/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.WindowConstants;
import ucr.ac.cr.modelo.AreaJuego;
import ucr.ac.cr.modelo.Datos;
import ucr.ac.cr.modelo.Personaje;
import ucr.ac.cr.modelo.RegistroDatosXML;
import ucr.ac.cr.modelo.Tiempo;
import ucr.ac.cr.vista.GUIGuia;
import ucr.ac.cr.vista.GUIEstadistica;
import ucr.ac.cr.vista.GUIMenu;
import ucr.ac.cr.vista.PanelEstadistica;

/**
 *
 * @author Fabricio
 */
public class ControladorEstadistica implements ActionListener, MouseListener {

    private GUIEstadistica guiEstadistica;
    private PanelEstadistica panelEstadistica;
    private Personaje principal;
    private RegistroDatosXML registroDatos;
    private AreaJuego areaJuego;
    private Datos datos;

    public ControladorEstadistica(GUIEstadistica guiEstadistica, PanelEstadistica panelEstadistica) {
        registroDatos = new RegistroDatosXML();
        this.guiEstadistica = guiEstadistica;
        this.panelEstadistica = panelEstadistica;
        this.panelEstadistica.setDatosTablaEstadistica(registroDatos.getDatosTabla());

    }

    public void actionPerformed(ActionEvent e) {
        String valor = e.getActionCommand();

        switch (valor) {
            case "Agregar":

                String nombre = panelEstadistica.getTxtNombre();
                String apodo = panelEstadistica.getTxtApodo();
                int oro = guiEstadistica.principal.getOro();
                int vidas = guiEstadistica.principal.getVidas();

                if (nombre.equals("") || apodo.equals("")) {
//                    ("Por favor, llene los espacios correspondientes");
                } else {
                    datos = registroDatos.buscar(panelEstadistica.getTxtNombre());

                    if (datos == null) {
                        datos = new Datos(nombre, apodo, oro, vidas);

                        registroDatos.agregar(datos);
                        
                        System.out.println("Nombre: " + nombre + " Apodo: " + apodo + " Oro: " + oro);

                        panelEstadistica.limpiarTodo();
                        panelEstadistica.setDatosTablaEstadistica(registroDatos.getDatosTabla());

                    }

                    panelEstadistica.limpiarTodo();

                }
            

        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.err.println(panelEstadistica.getFilaTabla());
        datos = (Datos) registroDatos.buscar(panelEstadistica.getFilaTabla());
        panelEstadistica.setTxtNombre(datos.getNombre());
        panelEstadistica.setTxtApodo(datos.getApodo());
       
         

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
